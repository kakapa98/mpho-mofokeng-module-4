import 'package:flutter/material.dart';
import 'package:flutter_application/Screens/mylogin.dart';
import 'package:flutter_application/widgets/myDashboard.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage("lib/assets/bg.jpeg"), fit: BoxFit.cover),
      ),
      child: Center(child: Column(children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: MaterialButton(
                height: 100.0,
                minWidth: 150.0,
                color: const Color.fromARGB(255, 145, 184, 6),
                textColor: Colors.white,
                child: const Text("Menu top left\n        or\n     Logout"),
                onPressed: () => {
                  Navigator.push(
                    context, MaterialPageRoute(
                      builder: (context) => const LogInScreen()))},
                )),
          ],
        )
      ],),),
      ),
      drawer: const NavDrawer(),
      appBar: AppBar(title: const Text('DashBoard'),
      
      ),
    );
  }
}
