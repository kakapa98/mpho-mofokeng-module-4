import 'package:flutter/material.dart';
import 'package:flutter_application/Screens/dashboard.dart';
import 'package:flutter_application/theme.dart';
import 'package:flutter_application/widgets/checkbox.dart';
import 'package:flutter_application/widgets/primary_button.dart';

class FeedBack extends StatelessWidget {
  const FeedBack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(138, 110, 100, 80),
      appBar: AppBar(
        title: const Text("App FeedBack"),
        centerTitle: true,
         elevation: 1,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Color.fromARGB(255, 7, 10, 7),
          ),
          onPressed: () => {Navigator.of(context).pop()},
        ),
      ),
    
      body: Center(
        child: Column(
          children: [
            const SizedBox(
              height: 70,
            ),
            Padding(
              padding: kDefaultPadding,
              child: Text(
                'How can you rate this APP?',
                style: titleText,
              ),
            ),
             const SizedBox(
              height: 20,
            ),
            const Padding(
              padding: kDefaultPadding,
              child: CheckBox('1 star rate.'),
            ),
            const SizedBox(
              height: 20,
            ),
            const Padding(
              padding: kDefaultPadding,
              child: CheckBox('2 star rate.'),
            ),
             const SizedBox(
              height: 20,
            ),
            const Padding(
              padding: kDefaultPadding,
              child: CheckBox('3 star rate.'),
            ),
            const SizedBox(
              height: 20,
            ),
            const Padding(
              padding: kDefaultPadding,
              child: CheckBox('4 star rate'),
            ),
             const SizedBox(
              height: 20,
            ),
            const Padding(
              padding: kDefaultPadding,
              child: CheckBox('5 star rate.'),
            ),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Dashboard(),
                  ),
                );
              },
              child: const Padding(
                padding: kDefaultPadding,
                child: PrimaryButton(buttonText: 'Submit'),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Dashboard(),
                  ),
                );
              },
              child: const Padding(
                padding: kDefaultPadding,
                child: PrimaryButton(buttonText: 'Cancel'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
