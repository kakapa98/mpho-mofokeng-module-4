import 'package:flutter/material.dart';

class Developer extends StatelessWidget {
  const Developer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("The Developer"),
        centerTitle: true,
         elevation: 1,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Color.fromARGB(255, 7, 10, 7),
          ),
          onPressed: () => {Navigator.of(context).pop()},
        ),
      ),
      body: Container(
          decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage("lib/assets/index.jpeg"), fit: BoxFit.cover),
        ),
      ),
    );
  }
}
